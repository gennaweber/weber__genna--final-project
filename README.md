# Personal Site

This app is hosted at [personal.gennaweber.me](http://personal.gennaweber.me).

The environment variable `REACT_APP_API` must be set when running yarn run start or yarn run build. For example, `REACT_APP_API=https://localhost:5000` if running the backend locally or `REACT_APP_API=https://personal-api-etgn7oeo2q-nn.a.run.app` if using the API hosted via Cloud Run.

The backend server repo for this project can be found at [https://gitlab.com/gennaweber/personal-site-node](https://gitlab.com/gennaweber/personal-site-node)

The database info for this project can be found at [https://gitlab.com/gennaweber/personal-site-database](https://gitlab.com/gennaweber/personal-site-database)

To access the protected /entries & /admin routes you can use the login:
email: gennamweber@gmail.com
password: password

You can also create a new user with your own credentials.

Go to the /admin route to perform CRUD operations on the content for the website.
